/*
 * Copyright (C) 2015 - 2023. Henrik Bærbak Christensen, Aarhus University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package cloud.cave.proxy;

import cloud.cave.common.ClientHelperMethods;
import cloud.cave.common.HelperMethods;

import cloud.cave.domain.*;
import cloud.cave.doubles.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.*;
/**
 * Test that the Broker chain works for the methods of Cave.
 * 
 * @author Henrik Baerbak Christensen, Aarhus University.
 * 
 */
public class TestCaveProxy {

  private Cave cave;

  @BeforeEach
  public void setup() {
    cave = ClientHelperMethods.createCaveProxyForTesting();
  }

  @Test
  public void shouldAllowLogin() {
    // Given a proxy for Cave, coupled to the servant
    // When I call the login method
    Player player = cave.login(TestConstants.MIKKEL_AARSKORT, TestConstants.MIKKEL_PASSWORD);
    // Then a valid player object is returned, indicating the parameters and return
    // values are correctly transferred
    assertThat(player, is(notNullValue()));

    assertThat(player.getAuthenticationStatus(), is(LoginResult.LOGIN_SUCCESS));
    assertThat(player.getID(), is("user-001"));
    assertThat(player.getName(), is("Mikkel"));

    // When trying to log in with wrong credentials
    player = cave.login(TestConstants.MIKKEL_AARSKORT, "wrong");
    // Then proper values are transferred
    assertThat(player.getAuthenticationStatus(), is(LoginResult.LOGIN_FAILED_UNKNOWN_SUBSCRIPTION));
  }
  
  @Test
  public void shouldAllowLogout() {
    // Given two players in cave
    Player p1 = HelperMethods.loginPlayer(cave, TestConstants.MAGNUS_AARSKORT);
    Player p2 = HelperMethods.loginPlayer(cave, TestConstants.MATHILDE_AARSKORT);
    // When I logout Mathilde via the Broker chain
    LogoutResult result = cave.logout(p2.getID());
    // Then result is correctly transferred
    assertThat(result, is(LogoutResult.SUCCESS));

    // When I logout unknown player
    result = cave.logout("unknown-id");
    // Then result is correctly transferred
    assertThat(result, is(LogoutResult.PLAYER_UNKNOWN));
  }

  @Test
  public void shouldSupportConfigurationDescription() {
    String desc = cave.describeConfiguration();
    assertThat(desc, is(notNullValue()));
    assertThat(desc, containsString("CaveServant configuration:"));
  }
}
