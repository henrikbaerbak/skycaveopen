/*
 * Copyright (C) 2015 - 2023. Henrik Bærbak Christensen, Aarhus University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package cloud.cave.server;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.*;

import cloud.cave.config.ObjectManager;
import cloud.cave.doubles.TestConstants;

import cloud.cave.common.*;
import cloud.cave.domain.*;
import cloud.cave.server.common.Point3;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

/** Test server side implementation of the Player abstraction.
 * 
 * On the server side, a player object directly communicate
 * with the storage layer in order to modify its state.
 * 
 * Most of these tests are the results of TDD. 
 * 
 * Many of the 'later' tests are abstracted into
 * static methods in CommonPlayerTests to allow
 * the same tests to be run using the client side
 * proxies as well to test the Broker handling
 * all method calls correctly.
 *
 * Some methods are tested in separate test classes
 * as they serve as exercises: Quote, Wall, Room handling.
 * 
 * @author Henrik Baerbak Christensen, Aarhus University.
 *
 */
public class TestPlayerServant {

  private Cave cave;
  private Player player;
  
  private String description;

  @BeforeEach
  public void setup() {
    ObjectManager objMgr = HelperMethods.createTestDoubledConfiguredCave();
    cave = objMgr.getCave();
    player = HelperMethods.loginPlayer(cave, TestConstants.MIKKEL_AARSKORT);
  }
 
  // TDD of simple player attributes
  @Test
  public void shouldAccessSimpleAttributes() {
    assertThat(player.getName(), is("Mikkel"));
    assertThat(player.getID(), is("user-001"));
    assertThat(player.getRegion(), is(Region.AARHUS));
    assertThat("(0,0,0)", player.getPosition(), is("(0,0,0)"));

  }

  // TDD room description
  @Test
  public void shouldHaveInitialLocation() {
    description = player.getShortRoomDescription();
    assertThat(description, is("You are standing at the end of a road before a small brick building."));
  }

  // TDD the movement of a player
  @Test
  public void shouldAllowNorthOneMove() {
    player.move(Direction.NORTH);
    description = player.getShortRoomDescription();
    assertThat(description, is("You are in open forest, with a deep valley to one side."));
    // Move back again
    player.move(Direction.SOUTH);
    description = player.getShortRoomDescription();
    assertThat(
        description, is("You are standing at the end of a road before a small brick building."));
  }
  
  // TDD movement of player
  @Test
  public void shouldAllowEastWestMoves() {
    player.move(Direction.EAST);
    description = player.getShortRoomDescription();
    assertThat(description,
            is("You are inside a building, a well house for a large spring."));
    
    player.move(Direction.WEST);
    description = player.getShortRoomDescription();
    assertThat(description,
            is("You are standing at the end of a road before a small brick building."));

    player.move(Direction.WEST);
    description = player.getShortRoomDescription();
    assertThat(description,
            is("You have walked up a hill, still in the forest."));

    player.move(Direction.EAST);
    description = player.getShortRoomDescription();
    assertThat(description,
            is("You are standing at the end of a road before a small brick building."));
  }

  // Handle illegal moves, trying to move to non-existing room
  @Test
  public void shouldNotAllowMovingSouth() {
    UpdateResult canMove = player.move(Direction.SOUTH);
    assertThat("It should not be possible to move south, no node there",
            canMove, is(UpdateResult.FAIL_AS_NOT_FOUND) );
  }
  
  // TDD the behavior for changing the (x,y,z) coordinates
  // during movement
  @Test
  public void shouldTestCoordinateTranslations() {
    assertThat(player.getPosition(), is("(0,0,0)"));

    player.move(Direction.NORTH);
    assertThat(player.getPosition(), is("(0,1,0)"));
    player.move(Direction.SOUTH);
    assertThat(player.getPosition(), is("(0,0,0)"));

    player.move(Direction.UP);
    assertThat(player.getPosition(), is("(0,0,1)"));
    player.move(Direction.DOWN);
    assertThat(player.getPosition(), is("(0,0,0)"));

    player.move(Direction.WEST);
    assertThat(player.getPosition(), is("(-1,0,0)"));

    player.move(Direction.EAST);
    assertThat(player.getPosition(), is("(0,0,0)"));
  }
  
  // TDD digging new rooms for a player
  @Test
  public void shouldAllowPlayerToDigNewRooms() {
    UpdateResult updateResult = player.digRoom(Direction.DOWN, "Road Cellar");
    assertThat(updateResult, is(UpdateResult.UPDATE_OK));

    UpdateResult valid = player.move(Direction.DOWN);
    String roomDesc = player.getShortRoomDescription();
    assertThat(roomDesc, containsString("Road Cellar"));
  }

  // Cannot dig a node in a direction where a node already exists
  @Test
  public void shouldNotAllowDigAtEast() {
    UpdateResult allowed = player.digRoom(Direction.EAST, "Santa's cave.");
    assertThat(allowed, is(not(UpdateResult.UPDATE_OK)));
  }
  
  // TDD of get exits, a validate that the
  // the long description is correct
  @Test
  public void shouldShowExitsForPlayersPosition() {
    List<Direction> exits = player.getExitSet();
    assertThat(exits, hasItems(Direction.NORTH, Direction.WEST, Direction.EAST, Direction.UP));
    assertThat(exits, not(hasItem( Direction.SOUTH)));
    assertThat(exits.size(), is(4));

    // move east, which only as one exit, back west
    player.move(Direction.EAST);
    exits = player.getExitSet();
    assertThat(exits, hasItem(Direction.WEST));
    assertThat(exits.size(), is(1));
  }

  // TDD of get exits
  @Test
  public void shouldShowValidExitsFromEntryRoom() {
    List<Direction> exitSet = player.getExitSet();
    assertThat(exitSet.size(), is(4));

    assertThat(exitSet, hasItem(Direction.NORTH));
    assertThat(exitSet, hasItem(Direction.WEST));
    assertThat(exitSet, hasItem(Direction.UP));
    assertThat(exitSet, hasItem(Direction.EAST));
  }

  // TDD of the long room description
  @Test
  public void shouldProvideLongDescription() {
    List<String> longDescription = player.getLongRoomDescription();

    assertThat(longDescription.get(1), containsString("Creator: Will Crowther, just now."));

    assertThat(longDescription.get(2), containsString("There are exits in"));
    assertThat(longDescription.get(3), containsString("NORTH"));
    assertThat(longDescription.get(3), containsString("WEST"));
    assertThat(longDescription.get(3), containsString("EAST"));

    assertThat(longDescription.get(4), containsString("You see other players:"));
    assertThat(longDescription.get(5), containsString("[0] Mikkel"));
  }



  // Positions of players are stored across logins
  @Test
  public void shouldBeAtPositionOfLastLogout() {
    // Log mathilde into the cave, initial position is 0,0,0
    // as the database is reset
    Player player = HelperMethods.loginPlayer(cave, TestConstants.MATHILDE_AARSKORT);

    Point3 pos = new Point3(0, 0, 0);
    assertThat(player.getPosition(), is(pos.getPositionString()));

    // Move mathilde
    player.move(Direction.EAST);
    String newPos = player.getPosition();

    // Log her out
    LogoutResult logoutResult = cave.logout(player.getID());

    assertThat(logoutResult, is(LogoutResult.SUCCESS));

    // Log her back in
    player = HelperMethods.loginPlayer(cave, TestConstants.MATHILDE_AARSKORT);

    // and verify she is in the place where she left
    assertThat(player.getPosition(), is(newPos));
  }

  // TDD of session id, later changed to an access token
  @Test
  public void shouldAssignUniqueAccessTokenForEveryLogin() {
    // The session id should be a new ID for every session 
    // (a session lasts from when a player logs in until he/she
    // logs out).
    String originalAccessToken = player.getAccessToken();
    String playerId = player.getID();
    assertThat(originalAccessToken, is(notNullValue()));
    
    // Do a second login and ensure that it gets a new session id
    Player p1 = HelperMethods.loginPlayer(cave, TestConstants.MIKKEL_AARSKORT);
    
    // It should be the same player, now double "logged in"
    // note, cannot call player.getID() as this will throw an
    // access control exception
    assertThat(playerId, is(p1.getID()));
    
    // But the session id is different
    assertThat(p1.getAccessToken(), is(not(originalAccessToken)));
  }
  
  // Test just to increase coverage :)
  @Test
  public void shouldReturnReasonableToString() {
    assertThat(player.toString(), containsString("storage=FakeCaveStorage"));
    assertThat(player.toString(), containsString("name='Mikkel'"));
    assertThat(player.toString(), containsString("ID='user-001'"));
    assertThat(player.toString(), containsString("region=AARHUS"));
  }
}
