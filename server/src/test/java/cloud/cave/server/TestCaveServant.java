/*
 * Copyright (C) 2015 - 2023. Henrik Bærbak Christensen, Aarhus University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package cloud.cave.server;

import cloud.cave.config.CaveServerFactory;
import cloud.cave.config.ObjectManager;
import cloud.cave.config.StandardObjectManager;
import cloud.cave.doubles.AllTestDoubleFactory;
import cloud.cave.doubles.TestStubSubscriptionService;
import cloud.cave.server.common.SubscriptionRecord;
import cloud.cave.service.SubscriptionService;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.*;

import cloud.cave.doubles.TestConstants;

import cloud.cave.common.*;
import cloud.cave.domain.*;
import jakarta.servlet.http.HttpServletResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/** Test cases for the server side implementation of the
 * Cave. Heavy use of test doubles to avoid all dependencies
 * to external services.
 * 
 * @author Henrik Baerbak Christensen, Aarhus University.
 *
 */
public class TestCaveServant {
  
  private Cave cave;
  
  private Player p1, p2;

  @BeforeEach
  public void setup() {
    cave = HelperMethods.createTestDoubledConfiguredCave().getCave();
  }

  @Test
  public void shouldAllowAddingPlayers() {
    // One player
    Player p1 = HelperMethods.loginPlayer(cave, TestConstants.MAGNUS_AARSKORT);
    assertThat(p1, is(notNullValue()));
    assertThat(p1.getID(), is("user-002"));
    assertThat( p1.getName(), is("Magnus"));
    assertThat( p1.getRegion(), is(Region.COPENHAGEN));

    // Enter Mathilde
    Player p2 = HelperMethods.loginPlayer(cave, TestConstants.MATHILDE_AARSKORT);
    assertThat(p2, is(notNullValue()));
    assertThat(p2.getID(), is("user-003"));
    assertThat( p2.getName(), is("Mathilde"));
  }
  
  @Test
  public void shouldRejectUnknownSubscriptions() {
    Player p = cave.login( "bandit@cs.au.dk", "wrongkey");
    assertThat(p.getAuthenticationStatus(), is(LoginResult.LOGIN_FAILED_UNKNOWN_SUBSCRIPTION));

    p = cave.login( TestConstants.MAGNUS_AARSKORT, "wrongkey");
    assertThat(p, is(notNullValue()));
    assertThat(p.getAuthenticationStatus(), is(LoginResult.LOGIN_FAILED_UNKNOWN_SUBSCRIPTION) );
  }
  
  @Test
  public void shouldAllowLoggingOutMagnus() {
    enterBothPlayers();
    // When Magnus logs out
    LogoutResult result = cave.logout(p1.getID());
    // Then it is allowed
    assertThat(result, is(LogoutResult.SUCCESS));
  }

  @Test
  public void shouldHandleLoggingOutUnknownPlayer() {
    LogoutResult result = cave.logout("unknown-player-id");
    assertThat(result, is(LogoutResult.PLAYER_UNKNOWN));
  }

  @Test
  public void shouldNotAllowLoggingOutMathildeTwice() {
    enterBothPlayers();
    // When log out Mathilde first time
    LogoutResult result = cave.logout(p2.getID());
    // Then it succeeds
    assertThat(result, is(LogoutResult.SUCCESS));

    // When a second logout occurs
    result = cave.logout(p2.getID());
    // Then the player is not in the cave
    assertThat(result, is(LogoutResult.PLAYER_NOT_IN_CAVE));
  }
  
  @Test
  public void shouldWarnIfMathildeLogsInASecondTime() {
    enterBothPlayers();
    // When trying to login mathilde a second time
    Player p = null;
    p = cave.login(TestConstants.MATHILDE_AARSKORT, TestConstants.MATHILDE_PASSWORD);
    // Then the login should be successfull but a warning should be issued of potentially
    // more than one client operating the played
    assertThat(p.getAuthenticationStatus(), is(LoginResult.LOGIN_SUCCESS_PLAYER_ALREADY_LOGGED_IN));

    assertThat(p, is(notNullValue()));
    assertThat( p.getID(), is("user-003"));
  }
  
  private void enterBothPlayers() {
    p1 = HelperMethods.loginPlayer(cave, TestConstants.MAGNUS_AARSKORT);
    p2 = HelperMethods.loginPlayer(cave, TestConstants.MATHILDE_AARSKORT);
  }

  @Test
  public void shouldDescribeConfiguration() {
    String configString = cave.describeConfiguration();
    assertThat(configString, is(notNullValue()));
    assertThat(configString, containsString("CaveStorage: cloud.cave.doubles.FakeCaveStorage"));
    assertThat(configString, containsString("SubscriptionService: cloud.cave.doubles.TestStubSubscriptionService"));
    assertThat(configString, containsString("PlayerNameService: cloud.cave.server.InMemoryNameService"));
  }

  @Test
  public void shouldNotLoginWhenInternalDaemonErrorOccurs() {
    // Given a Factory whose SubscriptionConnector is a Saboteur that
    // throws 500 internal server error
    CaveServerFactory factory = new AllTestDoubleFactory() {
      public SubscriptionService createSubscriptionServiceConnector(ObjectManager objMgr) {
        SubscriptionService service = new TestStubSubscriptionService() {

          @Override
          public SubscriptionRecord authorize(String loginName, String password) {
            return new SubscriptionRecord(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
          }
        };
        service.initialize(objMgr, null); // no config object required for the stub
        return service;
      }};
    ObjectManager objMgr = new StandardObjectManager(factory);
    // Given a Cave that will error internally on any login attempt
    cave = objMgr.getCave();

    // When Mikkel tries to log in
    Player p1 = cave.login(TestConstants.MIKKEL_AARSKORT, TestConstants.MIKKEL_PASSWORD);
    // Then a non-null player object is returned
    assertThat(p1, is(notNullValue()));
    // Then its auth status is 'failed due to server error'
    assertThat(p1.getAuthenticationStatus(), is(LoginResult.LOGIN_FAILED_SERVER_ERROR));
  }
}
